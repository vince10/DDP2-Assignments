package javari.park;

import java.util.ArrayList;
import java.util.List;

/**
 * Construct an instance of {@code Registration}
 * @param RegistrationId = customer's registration id
 * @param VisitorName = Store visitor's name
 * @param SelectedAttractions = Store Customer's Attraction
 *
 */
public class Register implements Registration {
    private int RegistrationId = 0;
    private String VisitorName;
    private List<SelectedAttraction> SelectedAttractions;

    public Register(int RegistrationId,String VisitorName){
        this.RegistrationId=0+RegistrationId;
        this.VisitorName=VisitorName;
        this.SelectedAttractions=new ArrayList<>();
    }

    public Register(String VisitorName){
        //this.RegistrationId=0+RegistrationId;
        this.VisitorName=VisitorName;
        this.SelectedAttractions=new ArrayList<>();
    }


    @Override
    public int getRegistrationId() {
        return RegistrationId;
    }

    @Override
    public String getVisitorName() {
        return VisitorName;
    }

    @Override
    public String setVisitorName(String name) {
        return VisitorName;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return SelectedAttractions;
    }

    public void setSelectedAttractions(List<SelectedAttraction> selectedAttractions) {
        SelectedAttractions = selectedAttractions;
    }

    public void setRegistrationId(int registrationId) {
        RegistrationId = registrationId;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        getSelectedAttractions().add(selected);
        setSelectedAttractions(getSelectedAttractions());
        return true;
    }
}
