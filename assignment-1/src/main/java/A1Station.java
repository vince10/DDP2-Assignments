import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int numberofcats = Integer.parseInt(input.nextLine());
        boolean caronthetrack = false; // whether there is a car or not
        TrainCar currentrain = null; // the cat enter this car
        TrainCar previoustrain = null; // most recent occupied
        int n = 0; // the number of most recent departed cat
        double remnants = 0; // remnants of cat
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);

        for (double i = 1; i < numberofcats + 1; i++) {
            String[] command = input.nextLine().split(",");
            String catname = command[0];
            double catweight = Double.parseDouble(command[1]);
            double catlength = Double.parseDouble(command[2]);
            WildCat thecat = new WildCat(catname, catweight, catlength);
            if (!caronthetrack) {
                currentrain = new TrainCar(thecat);
                caronthetrack = true;
                previoustrain = currentrain;
            } else {
                currentrain = new TrainCar(thecat, previoustrain);
                previoustrain = currentrain;
                if (currentrain.computeTotalWeight() >= THRESHOLD) { //Depart the train
                    remnants = i-n;
                    System.out.println("The train departs to Javari Park");
                    System.out.print("[LOCO]<--");
                    currentrain.printCar();// callinf recursive method
                    System.out.println("Average mass index of all cats: " + df.format(currentrain.computeTotalMassIndex() / remnants));
                    //if branching to determine average weight
                    if (currentrain.computeTotalMassIndex() / remnants < 18.5)
                        System.out.println("In average the cat in the train are *underweight*");
                    else if (18.5 <= currentrain.computeTotalMassIndex() / remnants && currentrain.computeTotalMassIndex() / remnants < 25)
                        System.out.println("In average the cat in the train are *normal*");
                    else if (25 <= currentrain.computeTotalMassIndex() / remnants && currentrain.computeTotalMassIndex() / remnants < 30)
                        System.out.println("In average the cat in the train are *overweight*");
                    else if (currentrain.computeTotalMassIndex() / remnants >= 30)
                        System.out.println("In average the cat in the train are *obese*");
                    caronthetrack = false;
                    previoustrain = null;
                    n = (int) i;
                }

            }



        }
        remnants = numberofcats - n;


        if (caronthetrack) {
            System.out.println("The train departs to Javari Park");
            System.out.print("[LOCO}<--");
            currentrain.printCar();
            System.out.println("Average mass index of all cats: " + df.format(currentrain.computeTotalMassIndex() / remnants));
            if (currentrain.computeTotalMassIndex() / remnants < 18.5)
                System.out.println("In average the cat in the train are *underweight*");
            else if (18.5 <= currentrain.computeTotalMassIndex() / remnants && currentrain.computeTotalMassIndex() / remnants < 25)
                System.out.println("In average the cat in the train are *normal*");
            else if (25 <= currentrain.computeTotalMassIndex() / remnants && currentrain.computeTotalMassIndex() / remnants < 30)
                System.out.println("In average the cat in the train are *overweight*");
            else if (currentrain.computeTotalMassIndex() / remnants >= 30)
                System.out.println("In average the cat in the train are *obese*");


        }


    }
}
