package javari.animal;

public class Mammal extends Animal {
    String specialcase = "";
    public Mammal(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    public Mammal(Integer id, String type, String name, Gender gender, double length, double weight,String specialcase, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialcase=specialcase;
    }

    public Mammal(){};

    @Override
    protected boolean specificCondition() {
        if (specialcase.equals("pregnant")) return true;
        return false;
    }

}
