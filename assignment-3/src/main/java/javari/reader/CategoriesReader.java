package javari.reader;

import javari.animal.Animal;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriesReader extends CsvReader {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     * @param invalid_animals = stores invalid animals ( to be used in AnimalRecordsReader object)
     * @param invalids = stores invalid categories
     */
    protected List<String> invalid_animals;
    protected List<String> invalids;

    public CategoriesReader(Path file) throws IOException {
        super(file);
        this.invalid_animals=new ArrayList<>();
        this.invalids=new ArrayList<>();
    }

    public void setInvalid_animals(List<String> invalid_animals) {
        this.invalid_animals = invalid_animals;
    }

    public List<String> getInvalid_animals() {
        return invalid_animals;
    }

    public List<String> getInvalids() {
        return invalids;
    }

    public void setInvalids(List<String> invalids) {
        this.invalids = invalids;
    }

    @Override
    public long countValidRecords() {
        int count =0;
        List<String> valid_categories = new ArrayList<>();
        for (String s: getLines()){
            String [] temp = s.split(",");

            switch (temp[1]){
                case "mammals":
                    List<String> mammals_list = new ArrayList<>(Arrays.asList("Hamster","Lion","Cat","Whale"));
                    if (!mammals_list.contains(temp[0])) {
                        if (!getInvalids().contains(temp[1])){
                            getInvalids().add(temp[1]);
                            setInvalids(getInvalids());
                            getInvalid_animals().add(temp[0]);
                            setInvalid_animals(getInvalid_animals());
                        }
                        //count-=1;
                    }
                    else{
                        /*getValid_animals().add(temp[0]);
                        setValid_animals(getValid_animals());*/
                        if (!valid_categories.contains(temp[1])){
                            valid_categories.add(temp[1]);
                        }
                    }

                    break;

                case "reptiles":
                    List<String> reptiles_list = new ArrayList<>(Arrays.asList("Snake"));
                    if (!reptiles_list.contains(temp[0])) {
                        if (!getInvalids().contains(temp[1])){
                            getInvalids().add(temp[1]);
                            setInvalids(getInvalids());
                            getInvalid_animals().add(temp[0]);
                            setInvalid_animals(getInvalid_animals());
                        }
                        //count-=1;
                    }
                    else{
                        /*getValid_animals().add(temp[0]);
                        setValid_animals(getValid_animals());*/
                        if (!valid_categories.contains(temp[1])){
                            valid_categories.add(temp[1]);
                        }
                    }
                    break;
                case "aves":
                    List<String> aves_list = new ArrayList<>(Arrays.asList("Eagle","Parrot"));
                    if (!aves_list.contains(temp[0])) {
                        if (!getInvalids().contains(temp[1])){
                            getInvalids().add(temp[1]);
                            setInvalids(getInvalids());
                            getInvalid_animals().add(temp[0]);
                            setInvalid_animals(getInvalid_animals());
                        }
                        //count-=1;
                    }
                    else{
                        /*getValid_animals().add(temp[0]);
                        setValid_animals(getValid_animals());*/
                        if (!valid_categories.contains(temp[1])){
                            valid_categories.add(temp[1]);
                        }
                    }
                    break;
                default:
                    if (!getInvalids().contains(temp[1])){
                        getInvalids().add(temp[1]);
                        setInvalids(getInvalids());
                        getInvalid_animals().add(temp[0]);
                        setInvalid_animals(getInvalid_animals());
                    }
            }

        }
        /*for(String s : getLines()){
            String [] temp = s.split(",");
            if(!getInvalids().contains(temp[1])){
                getValid_animals().add(temp[0]);
                setValid_animals(getValid_animals());
            }

        }*/


        return valid_categories.size();
    }

    @Override
    public long countInvalidRecords() {
        return getInvalids().size();
    }


}
