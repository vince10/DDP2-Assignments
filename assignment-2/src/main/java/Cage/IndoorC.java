package Cage;

import Animals.*;

public class IndoorC extends Cage {
    private final long width = 60;
    private final long length = 120;

    public IndoorC(Animals animals) {
        super(animals);
    }

    public String info() {
        return getAnimal().getName() + " ( " + getAnimal().getLenght() + " - C)";
    }
}
