import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Footer extends JPanel {
    private Reset reset;
    private JButton exit_button;
    private JLabel label;
    private int height;
    private int width;
    private GamePanel gamePanel;

    /**
     *
     * @param height =  Height of footer
     * @param width = Width of footer
     * @param gamePanel = GamePanel
     */
    public Footer(int height, int width, GamePanel gamePanel){
        this.height=height;
        this.width=width;
        this.gamePanel=gamePanel;
        this.setSize(gamePanel.getWidth(), 100);
        this.setLayout(new FlowLayout());
        this.setVisible(true);
        this.reset=null;
        this.exit_button=null;
        this.label=null;


    }

    public void setExit_button(JButton exit_button) {
        this.exit_button = exit_button;
        this.exit_button.setVisible(true);
    }

    public JButton getExit_button() {
        return exit_button;
    }


    public void setReset(Reset reset) {
        this.reset = reset;
        this.reset.setVisible(true);
    }

    public Reset getReset() {
        return reset;
    }

    public void setLabel(JLabel label) {
        this.label = label;
        this.label.setVisible(true);
    }

    public JLabel getLabel() {
        return label;
    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }

    /**
     * Create Exit Button with customized Font
     */

    public void create_exit_button(){
        JButton exit = new JButton("Exit");
        exit.setFont(new Font("Montserrat",Font.PLAIN,25));
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        setExit_button(exit);
        this.add(getExit_button());
    }

    /**
     * Create reset button
     */
    public void create_reset(){
        Reset reset = new Reset(getGamePanel());
        setReset(reset);
        this.add(getReset());
    }

   public void create_label(){
        this.add(getGamePanel().getLabel());
    }



}
