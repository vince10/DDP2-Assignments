package Cage;

import Animals.*;

public class OutdoorA extends Cage {
    private final long width = 120;
    private final long length = 120;

    public OutdoorA(Animals animals) {
        super(animals);
    }

    public String info() {
        return getAnimal().getName() + " ( " + getAnimal().getLenght() + " - A)";
    }

}
