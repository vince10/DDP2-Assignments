package javari.animal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Lion extends Mammal {
    private final List<String> attractions = new ArrayList<>(Arrays.asList("Circles of Fires"));
    public Lion(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    public Lion(Integer id, String type, String name, Gender gender, double length, double weight, String specialcase, Condition condition) {
        super(id, type, name, gender, length, weight, specialcase, condition);
    }

    @Override
    protected boolean specificCondition() {
        if (getGender().equals(Gender.FEMALE)) return true;
        if (specialcase.equals("pregnant")) return true;
        return false;
    }

    public List<String> getAttractions() {
        return attractions;
    }
}
