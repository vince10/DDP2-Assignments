import javax.swing.*;
import java.awt.*;


public class MemoryGame {
    /**
     * Create the game with the following specification:
     * Frame size = 920 x 720
     * Frame Layout = Border Layout
     */
    public static void create_the_game(){
        JFrame frame = new JFrame();
        frame.setSize(720,920);
        frame.setTitle("Remeber The Cards !");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLayout(new BorderLayout());
        GamePanel tes = new GamePanel(720,720);
        tes.setVisible(true);
        Footer foot_1= new Footer(100,tes.getWidth(),tes);
        foot_1.create_reset();
        foot_1.create_exit_button();
        Footer foot_2 = new Footer(100,tes.getWidth(),tes);
        foot_2.create_label();
        frame.add(tes,BorderLayout.NORTH);
        frame.add(foot_1,BorderLayout.CENTER);
        frame.add(foot_2,BorderLayout.SOUTH);
        frame.setVisible(true);
    }
    public static void main(String[] args){
        create_the_game();
    }

}
