package javari.animal;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Parrot extends Aves {
    private final List<String> attractions = new ArrayList<>(Arrays.asList("Dancing Animals","Counting Masters"));
    public Parrot(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    public Parrot(Integer id, String type, String name, Gender gender, double length, double weight, String specialcase, Condition condition) {
        super(id, type, name, gender, length, weight, specialcase, condition);
    }

    public List<String> getAttractions() {
        return attractions;
    }
}
