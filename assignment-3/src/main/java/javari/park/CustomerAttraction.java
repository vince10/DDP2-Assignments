package javari.park;

import javari.animal.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Construct an instance for {@code CustomerAttraction}
 * Store 1 attraction with a list of animals as performers
 */
public class CustomerAttraction implements SelectedAttraction {
    private String Name;
    private List<Animal> Performers;

    public CustomerAttraction(String Name){
        this.Performers=new ArrayList<>();
        this.Name=Name;
    }

    @Override
    public String getName() {
        return Name;
    }

    @Override
    public String getType() {
        if (getPerformers().get(0) instanceof Hamster){
            return "Hamster";
        }
        else if (getPerformers().get(0) instanceof Lion){
            return "Lion";
        }
        else if (getPerformers().get(0) instanceof Cat){
            return "Cat";
        }
        else if (getPerformers().get(0) instanceof Whale){
            return "Whale";
        }
        else if (getPerformers().get(0) instanceof Parrot){
            return "Parrot";
        }
        else if (getPerformers().get(0) instanceof Eagle){
            return "Eagle";
        }
        else if (getPerformers().get(0) instanceof Snake){
            return "Snake";
        }


        return null;
    }

    @Override
    public List<Animal> getPerformers() {
        return Performers;
    }

    public void setPerformers(List<Animal> performers) {
        Performers = performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        if (!performer.isShowable()){
            return false;
        }
        getPerformers().add(performer);
        //setPerformers(getPerformers());
        return true;
    }
}
