package Animals;

public class Parrot extends Animals {
    private boolean wildanimal = false;

    public Parrot(String name, int length) {
        super(name, length);
    }

    public boolean isWildanimal() {
        return wildanimal;
    }

    public String terbang() {
        return "Parrot " + this.getName() + " flies!\n" +
                this.getName() + " makes a voice: FLYYYY…..\n" + "Back to the office! ";
    }

    public String conversation(String yourline) {
        if (yourline.equals("")) return this.getName() + " says: HM?\n"
                + "Back to the office!";
        return this.getName() + " says: " + yourline.toUpperCase()
                + "\nBack to the office!";
    }
}
