package Cage;
//import com.sun.deploy.util.ArrayUtil;

import Animals.*;

//import com.sun.deploy.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Cage {
    private Animals animal;

    public Cage(Animals animal) {
        this.animal = animal;
    }

    public Cage() {


    }


    public Animals getAnimal() {
        return animal;
    }

    public void setAnimal(Animals animal) {
        this.animal = animal;
    }


}
