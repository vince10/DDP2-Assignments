import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Reset extends JButton {
    private GamePanel gamePanel;

    /**
     * @param gamePanel = GamePanel
     * Set text and font for reset button
     *
     */
    public Reset (GamePanel gamePanel){
        this.gamePanel=gamePanel;
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getGamePanel().reset();
            }
        });
        this.setFont(new Font("Montserrat",Font.PLAIN,25));
        this.setText("Play Again ?");

    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }
}
