package Cage;

import Animals.*;

public class OutdoorB extends Cage {
    private final long width = 120;
    private final long length = 150;

    public OutdoorB(Animals animals) {
        super(animals);
    }

    public String info() {
        return getAnimal().getName() + " ( " + getAnimal().getLenght() + " - B)";
    }

}
