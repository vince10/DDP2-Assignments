package Animals;

import java.util.Random;

public class Cat extends Animals {
    String[] expression = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    private boolean wildanimal = false;

    public Cat(String name, int length) {
        super(name, length);

    }

    public String Cuddle() {
        int num = new Random().nextInt(expression.length);
        return this.getName() + " makes a voice: " +
                expression[num] + "\n" +
                "Back to the office!";

    }

    public String Brushthefur() {
        return "Time to clean " + this.getName() +
                "'s fur \n" + this.getName() + " makes a voice: Nyaaan...\n" +
                "Back to the office!";
    }

    public boolean isWildanimal() {
        return wildanimal;
    }
}

