package javari.animal;

public class Aves extends Animal {

    String specialcase ="";
    public Aves(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    public Aves(Integer id, String type, String name, Gender gender, double length, double weight,String specialcase, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialcase=specialcase;
    }
    public Aves(){};

    @Override
    protected boolean specificCondition() {
        if (specialcase.equals("laying eggs")) return true;
        return false;
    }
}
