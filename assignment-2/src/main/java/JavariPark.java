import Animals.*;
import Cage.*;

import java.util.ArrayList;
import java.util.Scanner;




public class JavariPark {
    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park!\n" +
                "Input the number of animals!");
        Scanner input = new Scanner(System.in);
        System.out.print("cat: ");
        int numofcat = Integer.parseInt(input.nextLine());
        ArrayList<Animals> catlist = new ArrayList<>(numofcat);
        ArrayList<Cat> realcatlist = new ArrayList<>(numofcat);
        ArrayList<String> catnamelist = new ArrayList<>(numofcat);
        if (numofcat > 0) {
            System.out.print("Provide the information of cat(s): \n");
            String[] rawanimalist = input.nextLine().split(",");
            for (int i = 0; i < rawanimalist.length; i++) {
                String[] splitan = rawanimalist[i].split("\\|");
                String name = splitan[0];
                catnamelist.add(name);
                int length = Integer.parseInt(splitan[1]);
                Cat cat = new Cat(name, length);
                catlist.add(cat);
                realcatlist.add(cat);
            }
        }
        System.out.print("\nlion: ");
        int numoflion = input.nextInt();
        ArrayList<Animals> lionlist = new ArrayList<>(numoflion);
        ArrayList<Lion> reallionlist = new ArrayList<>(numofcat);
        ArrayList<String> lionnamelist = new ArrayList<>(numoflion);
        if (numoflion > 0) {
            System.out.print("Provide the information of lion(s): \n");
            String[] rawanimalist = input.next().split(",");

            for (int i = 0; i < rawanimalist.length; i++) {
                String name = rawanimalist[i].split("\\|")[0];
                lionnamelist.add(name);
                int length = Integer.parseInt(rawanimalist[i].split("\\|")[1]);
                Lion lion = new Lion(name, length);
                lionlist.add(lion);
                reallionlist.add(lion);
            }
        }

        System.out.print("\neagle: ");
        int numofeagle = input.nextInt();
        //System.out.print(numofeagle);
        ArrayList<Animals> eaglelist = new ArrayList<>(numofeagle);
        ArrayList<Eagle> realeaglelist = new ArrayList<>(numofeagle);
        ArrayList<String> eaglenamelist = new ArrayList<>(numofeagle);
        if (numofeagle > 0) {
            System.out.print("Provide the information of eagle(s): \n");
            String[] rawanimalist = input.next().split(",");

            for (int i = 0; i < rawanimalist.length; i++) {
                String name = rawanimalist[i].split("\\|")[0];
                eaglenamelist.add(name);
                int length = Integer.parseInt(rawanimalist[i].split("\\|")[1]);
                Eagle eagle = new Eagle(name, length);
                eaglelist.add(eagle);
                realeaglelist.add(eagle);
            }
        }
        System.out.print("\nparrot: ");
        int numofparrot = input.nextInt();
        ArrayList<Animals> parrotlist = new ArrayList<>(numofparrot);
        ArrayList<Parrot> realparrotlist = new ArrayList<>(numofparrot);
        ArrayList<String> parrotnamelist = new ArrayList<>(numofparrot);
        if (numofparrot > 0) {
            System.out.print("Provide the information of parrot(s): \n");
            String[] rawanimalist = input.next().split(",");

            for (int i = 0; i < rawanimalist.length; i++) {
                String name = rawanimalist[i].split("\\|")[0];
                parrotnamelist.add(name);
                int length = Integer.parseInt(rawanimalist[i].split("\\|")[1]);
                Parrot parrot = new Parrot(name, length);
                parrotlist.add(parrot);
                realparrotlist.add(parrot);
            }
        }
        System.out.print("\nhamster: ");
        int numofhamster = input.nextInt();
        ArrayList<Animals> hamsterlist = new ArrayList<>(numofhamster);
        ArrayList<Hamster> realhamsterlist = new ArrayList<>(numofhamster);
        ArrayList<String> hamsternamelist = new ArrayList<>(numofhamster);
        if (numofhamster > 0) {
            System.out.print("Provide the information of hamster(s): \n");
            String[] rawanimalist = input.next().split(",");

            for (int i = 0; i < rawanimalist.length; i++) {
                String name = rawanimalist[i].split("\\|")[0];
                hamsternamelist.add(name);
                int length = Integer.parseInt(rawanimalist[i].split("\\|")[1]);
                Hamster hamster = new Hamster(name, length);
                hamsterlist.add(hamster);
                realhamsterlist.add(hamster);
            }
        }
        System.out.println("Animals have been successfully recorded!\n" +
                "\n" +
                "=============================================\n" +
                "Cage arrangement:\n");
        if (numofcat > 0) {

            System.out.println(ExhibitManager.levelmanager(catlist, false));
        }
        if (numoflion > 0) {
            System.out.println(ExhibitManager.levelmanager(lionlist, true));
        }
        if (numofeagle > 0) {
            System.out.println(ExhibitManager.levelmanager(eaglelist, true));
        }
        if (numofparrot > 0) {
            System.out.println(ExhibitManager.levelmanager(parrotlist, false));
        }
        if (numofhamster > 0) {
            System.out.println(ExhibitManager.levelmanager(hamsterlist, false));
        }
        System.out.println("NUMBER OF ANIMALS:\n" +
                "cat:" + numofcat + "\n" +
                "lion:" + numoflion + "\n" +
                "parrot:" + numofparrot + "\n" +
                "eagle:" + numofeagle + "\n" +
                "hamster:" + numofhamster + "\n" +
                "\n" +
                "=============================================\n");
        while (true) {
            System.out.println("Which animal you want to visit?\n" +
                    "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int angka = input.nextInt();
            if (angka == 1) {
                System.out.print("Mention the name of cat you want to visit:");
                String target = input.next();
                if (catnamelist.contains(target)) {
                    int index = catnamelist.lastIndexOf(target);
                    System.out.print("You are visiting " + catnamelist.get(index) + " (cat) now, what would you like to do?\n" +
                            "1: Brush the fur 2: Cuddle\n");
                    int option = input.nextInt();
                    if (option == 2) {
                        System.out.println(realcatlist.get(index).Cuddle());

                    } else if (option == 1) {
                        System.out.println(realcatlist.get(index).Brushthefur());

                    } else {
                        System.out.println("You do nothing...\n" +
                                "Back to the office!\n");
                    }

                } else if (catnamelist.size() == 0 || !catnamelist.contains(target)) {
                    System.out.println("There is no cat with that name! Back to the office!\n");

                }

            } else if (angka == 2) {
                System.out.print("Mention the name of eagle you want to visit:");
                String target = input.next();
                if (eaglenamelist.contains(target)) {
                    int index = eaglenamelist.lastIndexOf(target);
                    System.out.print("You are visiting " + eaglenamelist.get(index) + " (eagle) now, what would you like to do?\n" +
                            "1: Order to fly\n");
                    int option = input.nextInt();
                    if (option == 1) {
                        System.out.println(realeaglelist.get(index).fly());
                    } else {
                        System.out.println("You do nothing...\n" +
                                "Back to the office!\n");
                    }

                } else if (eaglenamelist.size() == 0 || !eaglenamelist.contains(target)) {
                    System.out.println("There is no eagle with that name! Back to the office!\n");

                }


            } else if (angka == 3) {
                System.out.print("Mention the name of hamster you want to visit:");
                String target = input.next();
                if (hamsternamelist.contains(target)) {
                    int index = hamsternamelist.lastIndexOf(target);
                    System.out.print("You are visiting " + hamsternamelist.get(index) + " (hamster) now, what would you like to do?\n" +
                            "1: See it gnawing 2: Order to run in the hamster wheel\n");
                    int option = input.nextInt();
                    if (option == 1) {
                        System.out.println(realhamsterlist.get(index).gnaw());

                    } else if (option == 2) {
                        System.out.println(realhamsterlist.get(index).runinwheel());

                    } else {
                        System.out.println("You do nothing...\n" +
                                "Back to the office!\n");
                    }

                } else if (catnamelist.size() == 0 || !hamsternamelist.contains(target)) {
                    System.out.println("There is no hamster with that name! Back to the office!\n");

                }

            } else if (angka == 4) {
                System.out.print("Mention the name of parrot you want to visit:");
                String target = input.next();
                if (parrotnamelist.contains(target)) {
                    int index = parrotnamelist.lastIndexOf(target);
                    System.out.print("You are visiting " + parrotnamelist.get(index) + " (hamster) now, what would you like to do?\n" +
                            "1: Order to fly 2: Do conversation\n");
                    int option = input.nextInt();
                    if (option == 1) {
                        System.out.println(realparrotlist.get(index).terbang());

                    } else if (option == 2) {
                        Scanner input2 = new Scanner(System.in);
                        System.out.print("You say:  ");
                        String line = input2.nextLine();
                        System.out.println(realparrotlist.get(index).conversation(line));

                    } else {
                        System.out.println(realparrotlist.get(index).conversation(""));
                        ;
                    }

                } else if (catnamelist.size() == 0 || !hamsternamelist.contains(target)) {
                    System.out.println("There is no parrot with that name! Back to the office!\n");

                }

            } else if (angka == 5) {
                System.out.print("Mention the name of lion you want to visit:");
                String target = input.next();
                if (lionnamelist.contains(target)) {
                    int index = lionnamelist.lastIndexOf(target);
                    System.out.print("You are visiting " + lionnamelist.get(index) + " (lion) now, what would you like to do?\n" +
                            "1: See it hunting 2: Brush the mane 3: Disturb it\n");
                    int option = input.nextInt();
                    if (option == 1) {
                        System.out.println(reallionlist.get(index).hunting());

                    } else if (option == 2) {
                        System.out.println(reallionlist.get(index).manecleaning());

                    } else if (option == 3) {
                        System.out.println(reallionlist.get(index).disturb());

                    } else {
                        System.out.println("You do nothing...\n" +
                                "Back to the office!\n");
                    }

                } else if (catnamelist.size() == 0 || !hamsternamelist.contains(target)) {
                    System.out.println("There is no lion with that name! Back to the office!\n");

                }


            } else if (angka == 99) {
                break;

            } else {
                System.out.println("animals doesn't exists");
            }

        }
        System.exit(0);
/*Katty|42,Ringgo|78,Diva|41,Pikachu|46,Giant|72,Dreamy|60,Kitten|39,Gemes|76,Bilbo|50,Bimo|65
Simba|74,Mambo|80
Leo|55
Greeny|32
Rainbow|30,Ronald|25,Aster|27
*/

    }
}
