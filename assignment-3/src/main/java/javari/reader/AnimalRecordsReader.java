package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class AnimalRecordsReader extends CsvReader {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     * @param final_valid_animals = arraylist contain the animals that can be made
     * @param invalid_animals = accept list of invalid_animals from CategoriesReader object,
     *                        this is needed for comparison in order to generate final_valid_animal
     *                        later
     *
     *
     */
    protected List<String> final_valid_animals;
    protected List<String> invalid_animals;
    //protected List<String> final_invalid_animals;
    public AnimalRecordsReader(Path file) throws IOException {
        super(file);
        this.final_valid_animals= new ArrayList<>();
    }

    public AnimalRecordsReader(Path file, List<String> invalid_animals) throws IOException {
        super(file);
        this.invalid_animals=invalid_animals;
        this.final_valid_animals= new ArrayList<>();
    }

    public void setInvalid_animals(List<String> invalid_animals) {
        this.invalid_animals = invalid_animals;
    }

    public List<String> getInvalid_animals() {
        return invalid_animals;
    }
    /*public List<String> getFinal_invalid_animals() {
        return final_invalid_animals;
    }
    public void setFinal_invalid_animals(List<String> final_invalid_animals) {
        this.final_invalid_animals = final_invalid_animals;
    }*/

    public List<String> getFinal_valid_animals() {
        return final_valid_animals;
    }

    public void setFinal_valid_animals(List<String> final_valid_animals) {
        this.final_valid_animals = final_valid_animals;
    }

    @Override
    public long countValidRecords() {
        return getLines().size()-countInvalidRecords();
    }

    @Override
    public long countInvalidRecords() {
        int count=0;

        for (String s: getLines()){

            try{
                String[] temp = s.split(",");
                if (getInvalid_animals().contains(temp[0])){
                    count+=1;
                }

                double tes_double1=Double.parseDouble(temp[4]);
                double tes_double2=Double.parseDouble(temp[5]);


            }
            catch (NumberFormatException e){
                count+=1;
            }


        }
        return count;
    }

    /**
     * Generate list of string ( still raw from the .csv file) which contain the
     * number of animals who has the right so to speak to exists in Javari Park
     *
     */

    public void generate_valid_animals(){
        for (String s : getLines()){
            String [] temp = s.split(",");
            if (!getInvalid_animals().contains(temp[1])){
                getFinal_valid_animals().add(s);
                setFinal_valid_animals(getFinal_valid_animals());
            }
        }
    }


}
