package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AttractionsReader extends CsvReader {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     * @param invalids = list of invalid attractions
     * @param valid_attractions = list of valid attractions
     */
    protected List<String> invalids;
    protected  List<String> valid_attractions;
    public AttractionsReader(Path file) throws IOException {
        super(file);
        this.invalids= new ArrayList<>();
        this.valid_attractions= new ArrayList<>();
    }

    public void setInvalids(List<String> invalids) {
        this.invalids = invalids;
    }

    public List<String> getInvalids() {
        return invalids;
    }

    public void setValid_attractions(List<String> valid_attractions) {
        this.valid_attractions = valid_attractions;
    }

    public List<String> getValid_attractions() {
        return valid_attractions;
    }

    /**
     * Flow:
     * check an attractions and if invalid sends it to invalids
     * after that another loop to check if an atrraction is valid then store it to valid_attractions
     * @return number of valid attractions
     */

    @Override
    public long countValidRecords() {
       for (String s: getLines()){
           String[] temp = s.split(",");
           switch (temp[1]){
               case "Circles of Fires":
                   List<String> CoF_eligible = new ArrayList<>(Arrays.asList("Lion","Whale","Eagle"));
                   if (!CoF_eligible.contains(temp[0])) {
                       if (!getInvalids().contains(temp[1])){
                           getInvalids().add(temp[1]);
                           setInvalids(getInvalids());
                       }

                   }
                   break;
               case "Dancing Animals":
                   List<String> DA_eligible = new ArrayList<>(Arrays.asList("Parrot","Snake","Cat","Hamster"));
                   if (!DA_eligible.contains(temp[0])) {
                       if (!getInvalids().contains(temp[1])){
                           getInvalids().add(temp[1]);
                           setInvalids(getInvalids());
                       }
                   }
                   break;
               case "Counting Masters":
                   List<String> CM_eligible = new ArrayList<>(Arrays.asList("Hamster","Whale","Parrot"));
                   if (!CM_eligible.contains(temp[0])) {
                       if (!getInvalids().contains(temp[1])){
                           getInvalids().add(temp[1]);
                           setInvalids(getInvalids());
                       }
                   }
                   break;

               case "Passionate Coders":
                   List<String> PC_eligible = new ArrayList<>(Arrays.asList("Hamster","Cat","Snake"));
                   if (!PC_eligible.contains(temp[0])) {
                       if (!getInvalids().contains(temp[1])){
                           getInvalids().add(temp[1]);
                           setInvalids(getInvalids());
                       }

                   }
                   break;

               default :
                   if (!getInvalids().contains(temp[1])){
                       getInvalids().add(temp[1]);
                       setInvalids(getInvalids());
                   }

           }

       }

       for (String s : getLines()){
           String[] temp = s.split(",");
           if (!getInvalids().contains(temp[1])){
               if (!getValid_attractions().contains(temp[1])){
                   getValid_attractions().add(temp[1]);
                   setValid_attractions(getValid_attractions());
               }

           }
       }

       return getValid_attractions().size();
    }

    @Override
    public long countInvalidRecords() {
        return getInvalids().size();
    }
}
