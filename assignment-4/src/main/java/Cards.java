import javax.swing.*;
import java.awt.*;


public class Cards extends JButton{
    private boolean flipped;
    private ImageIcon side_1, side_2;
    private int num;
    private javax.swing.Timer time;

    /**
     *
     * @param path = String containing the path of ImageIcon
     * @param num = Card's id's in int
     * @param width = The width of the card
     * @param height = The height of the card
     * Resize the image from directory to fit within the button using line 24th and 25th code
     */
    public Cards (String path, int num,int width, int height){
        ImageIcon side_1_temp = new ImageIcon("D:\\KULIAH\\Semester 2\\DDP2\\programming assignments\\DDP2-Assignments\\assignment-4\\" +
                "img\\object\\side_1.png");
        ImageIcon side_2_temp = new ImageIcon(path);
        this.flipped = false;
        this.side_1 = new ImageIcon(side_1_temp.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
        this.side_2=  new ImageIcon(side_2_temp.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
        this.num=num;
        this.setIcon(getSide_1());
        this.setVisible(true);

    }

    public ImageIcon getSide_1() {
        return side_1;
    }

    public ImageIcon getSide_2() {
        return side_2;
    }

    public int getNum() {
        return num;
    }

    public void setFlipped(boolean flipped) {
        this.flipped = flipped;
    }

    public boolean isFlipped() {
        return flipped;
    }

    /**
     * Flip the cards
     */

    public void flip(){
        if (!isFlipped()){
            setIcon(getSide_2());
            setFlipped(true);
        }
        else{
            setIcon(getSide_1());
            setFlipped(false);
        }
    }




}


