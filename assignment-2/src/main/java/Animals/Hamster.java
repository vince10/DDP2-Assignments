package Animals;

public class Hamster extends Animals {
    private boolean wildanimal = false;

    public Hamster(String name, int length) {
        super(name, length);
    }

    public boolean isWildanimal() {
        return wildanimal;
    }

    public String gnaw() {
        return this.getName() + " makes a voice: ngkkrit.. ngkkrrriiit\nBack to the office!";
    }

    public String runinwheel() {
        return this.getName() + " makes a voice: trrr…. trrr...\nBack to the office!";
    }
}
