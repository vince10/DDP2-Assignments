import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;



public class GamePanel extends JPanel {
    private ArrayList<Cards> collections ;
    private javax.swing.Timer time;
    private String default_path = "D:\\KULIAH\\Semester 2\\DDP2\\programming assignments\\DDP2-Assignments\\assignment-4\\" +
            "img\\object\\";
    private Integer num = null;
    private int num_of_try = 0;
    private int width;
    private int height;
    private JLabel label;


    /**
     * Construct GamePanel with 6 x 6 GridLayout
     * @param  width =  set the with of the GamePanel
     * @param height = set the height of the GamePanel
     *
     *
     */
    public GamePanel( int width, int height){
        this.width=width;
        this.height=height;
        this.setBounds(0, 0, this.width, this.height);
        this.setLayout(new GridLayout(6,6));
        this.setVisible(true);
        this.collections =  new ArrayList<>();
        this.GenerateCards();
        this.label= new JLabel("Number of tries: ");
        this.label.setFont(new Font("Times New Roman",Font.PLAIN,25));


    }

    public JLabel getLabel() {
        return label;
    }


    /**
     * Generate and ArrayList of Cards with pre-determined size and also
     * equipped the cards with designated event handler ( cards.flip()
     * and CompareCards )
     * Set a Timer with 750 ms delay
     */

    public void GenerateCards(){
        int num = 1;

        for ( int k=0; k<2;k++){
            for (int i=0 ; i < 3; i++){
                for (int j=0; j<6;j++){
                    String path = this.default_path+num+".png";
                    Cards cards = new Cards(path,num,getWidth()/6,getHeight()/6);
                    cards.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvente) {
                            cards.flip();
                            Timer time = new Timer(750, new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    CompareCards();
                                }
                            });
                            time.setRepeats(false);
                            time.start();
                        }
                    });

                    this.collections.add(cards);
                    num+=1;
                }
            }
            num=1;
        }
        Collections.shuffle(this.collections);

        for (Cards card: this.collections){
            this.add(card);
        }

    }

    /**
     * Compare 2 cards which isFlipped() base on card's id's (in int)
     * temp = temporary ArrayList containing 2 cards for comparison
     * Set the number of attempts
     * If cards are equal setVisible(false)
     * else do nothing
     *
     */

    public void CompareCards(){
        Cards card_1 = null;
        Cards card_2 = null;
        ArrayList<Cards> temp =  new ArrayList<>();
        for (Cards cards : this.collections){
            if (cards.isFlipped()&&cards.isVisible()) {
                temp.add(cards);
            }
        }
        if (temp.size()==2){
            card_1 = temp.get(0);
            card_2 = temp.get(1);
            if (card_1.getNum()==card_2.getNum()){
                for (Cards cards: this.collections){
                    if (cards.isFlipped()) cards.setVisible(false);
                }
            }
            if (card_1.getNum()!=card_2.getNum()){
                for (Cards cards: this.collections){
                    if (cards.isFlipped()) {
                        cards.flip();
                    }
                }
            }
            this.num_of_try+=1;
            this.label.setText("Number of tries: "+this.num_of_try);
        }

    }

    /**
     * Reset the whole Game
     * Set isFlipped() to false
     * set number of try to 0
     */

    public void reset (){
        for (Cards cards: this.collections){
            cards.setVisible(false);
        }
        Collections.shuffle(this.collections);
        for (Cards cards: this.collections){
            cards.setVisible(true);
            this.add(cards);
            cards.setFlipped(false);
            ImageIcon default_image = cards.getSide_1();
            cards.setIcon(default_image);
            this.num_of_try=0;
            this.label.setText("Number of tries: ");
        }

    }








}
