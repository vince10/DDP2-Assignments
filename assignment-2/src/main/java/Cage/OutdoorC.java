package Cage;

import Animals.*;

public class OutdoorC extends Cage {
    private final long width = 120;
    private final long length = 180;

    public OutdoorC(Animals animals) {
        super(animals);
    }

    public String info() {
        return getAnimal().getName() + " ( " + getAnimal().getLenght() + " - C)";
    }


}
