package Animals;

public class Lion extends Animals {
    private boolean wildanimal = true;

    public Lion(String name, int length) {
        super(name, length);
    }

    public boolean isWildanimal() {
        return wildanimal;
    }

    public String hunting() {
        return "Lion is hunting..\n" +
                this.getName() + " makes a voice: err…!\n" +
                "Back to the office!";
    }

    public String manecleaning() {
        return "Clean the lion’s mane..\n" +
                this.getName() + " makes a voice: Hauhhmm!\n"
                + "Back to the office!";
    }

    public String disturb() {
        return this.getName() + " makes a voice: HAUHHMM!\n" +
                "Back to the office!";
    }
}
