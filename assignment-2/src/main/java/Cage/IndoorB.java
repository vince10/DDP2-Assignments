package Cage;

import Animals.*;

public class IndoorB extends Cage {

    private final long width = 60;
    private final long length = 90;

    public IndoorB(Animals animals) {
        super(animals);
    }

    public String info() {
        return getAnimal().getName() + " ( " + getAnimal().getLenght() + " - B)";
    }
}
