package Cage;

import Animals.Animals;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class ExhibitManager {

    public static String levelmanager(ArrayList<Animals> animalist, boolean wildanimal) {
        String motherstr = "";
        // Arraylist containing Arraylist of cage per level
        ArrayList<ArrayList<Cage>> zoocollection = new ArrayList<>(Arrays.asList(null, null, null));
        int index = 0; // for access arraylist of animals
        // This one will handle wildanimal
        if (wildanimal) {
            // Brute force to handle arrangement for just 1 animal
            if (animalist.size() == 1) {
                ArrayList<Cage> cagelvl1 = new ArrayList<>(Arrays.asList(new Cage(animalist.get(0))));
                zoocollection.set(1, cagelvl1);

                if (animalist.get(0).getLenght() < 75) {
                    motherstr += "location : outdoor\n" +
                            "level 3:\n" +
                            "level 2:\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), " + "\n" +
                            "level 1: " + "\n\n";
                } else if (animalist.get(0).getLenght() >= 75 && animalist.get(0).getLenght() <= 90) {
                    motherstr += "location : outdoor\n+" +
                            "level 3:\n" +
                            "level 2:\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), " + "\n" +
                            "level 1: " + "\n\n";
                } else {
                    motherstr += "location : outdoor\n+" +
                            "level 3:\n" +
                            "level 2:\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), " + "\n" +
                            "level 1: " + "\n\n";
                }
                //Brute force to handle if they are only 2 animals inside.

            } else if (animalist.size() == 2) {
                ArrayList<Cage> cagelvl1 = new ArrayList<>(Arrays.asList(new Cage(animalist.get(0))));
                ArrayList<Cage> cagelvl2 = new ArrayList<>(Arrays.asList(new Cage(animalist.get(1))));
                zoocollection.set(1, cagelvl1);
                zoocollection.set(0, cagelvl2);
                if (animalist.get(0).getLenght() < 75) {
                    motherstr += "location : outdoor\n" +
                            "level 3:\n" +
                            "level 2: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - A), " + "\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), \n\n" +
                            "After rearrangement...\n" +
                            "level 3: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - A), " + "\n" +
                            "level 2: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), " + "\n" +
                            "level 1: " + "\n\n";
                } else if (animalist.get(0).getLenght() >= 75 && animalist.get(0).getLenght() <= 90) {
                    motherstr += "location : outdoor\n+" +
                            "level 3:\n" +
                            "level 2: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - B), " + "\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), \n\n" +
                            "After rearrangement...\n" +
                            "level 3: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - B, " + "\n" +
                            "level 2: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), " + "\n" +
                            "level 1: " + "\n\n";
                } else {
                    motherstr += "location : outdoor\n+" +
                            "level 3:\n" +
                            "level 2: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - C), " + "\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:" + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - C, " + "\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), " + "\n" +
                            "level 1: " + "\n\n";
                }


            } else {
                // Handling the arrangement of cages per levels.
                for (int i = 1; i < 3; i++) {

                    ArrayList<Cage> levelcollection = new ArrayList<Cage>(animalist.size() / 3); // Arraylist of cage of level 1/2/3
                    String childstr = "level " + i + ": "; // a child string to be merged with it's mother string

                    // loop for adding animals and string assemblies
                    for (int j = 0; j < animalist.size() / 3; j++) {
                        if (animalist.get(index).getLenght() < 75) {
                            OutdoorA cage = new OutdoorA(animalist.get(index));
                            levelcollection.add(cage);
                            childstr += cage.info() + ", ";
                        } else if (75 <= animalist.get(index).getLenght() && animalist.get(index).getLenght() <= 90) {
                            OutdoorB cage = new OutdoorB(animalist.get(index));
                            levelcollection.add(cage);
                            childstr += cage.info() + ", ";

                        } else {
                            OutdoorC cage = new OutdoorC(animalist.get(index));
                            levelcollection.add(cage);
                            childstr += cage.info() + ", ";
                        }


                        index += 1;

                    }
                    motherstr = "\n" + childstr + motherstr;
                    // Imidiately after adding the animals the program directly assembles the cages to their corresponding levels
                    Collections.reverse(levelcollection);

                    if (i == 1) zoocollection.set(1, levelcollection);
                    if (i == 2) zoocollection.set(0, levelcollection);

                }
                // Special case for level 3, because this level may holds different number of animals compare to lvl 1 or 2
                ArrayList<Cage> levelcollection = new ArrayList<Cage>(animalist.size() - index);
                String childstr = "level 3: ";
                int left = animalist.size() - index;
                for (int j = 0; j < left; j++) {
                    if (animalist.get(index).getLenght() < 75) {
                        OutdoorA cage = new OutdoorA(animalist.get(index));
                        levelcollection.add(cage);
                        childstr += cage.info() + ", ";
                    } else if (75 <= animalist.get(index).getLenght() && animalist.get(index).getLenght() <= 90) {
                        OutdoorB cage = new OutdoorB(animalist.get(index));
                        levelcollection.add(cage);
                        childstr += cage.info() + ", ";

                    } else {
                        OutdoorC cage = new OutdoorC(animalist.get(index));
                        levelcollection.add(cage);
                        childstr += cage.info() + ", ";
                    }


                    index += 1;
                }
                Collections.reverse(levelcollection);
                zoocollection.set(2, levelcollection);
                motherstr = childstr + motherstr;
                motherstr = "location : outdoor\n" + motherstr;

                // Assemble the string after cages and levels new arrangement
                motherstr += "\n\nAfter rearrangement...\n";
                for (int i = 0; i < 3; i++) {
                    int level = 3 - i;
                    childstr = "level " + level + ": ";
                    for (int j = 0; j < zoocollection.get(i).size(); j++) {
                        if (zoocollection.get(i).get(j).getAnimal().getLenght() < 75) {
                            childstr += zoocollection.get(i).get(j).getAnimal().getName() + " (" +
                                    zoocollection.get(i).get(j).getAnimal().getLenght() + " - A), ";
                        } else if (zoocollection.get(i).get(j).getAnimal().getLenght() <= 90 && zoocollection.get(i).get(j).getAnimal().getLenght() >= 75) {
                            childstr += zoocollection.get(i).get(j).getAnimal().getName() + " (" +
                                    zoocollection.get(i).get(j).getAnimal().getLenght() + " - B), ";
                        } else {
                            childstr += zoocollection.get(i).get(j).getAnimal().getName() + " (" +
                                    zoocollection.get(i).get(j).getAnimal().getLenght() + " - C), ";
                        }


                    }
                    childstr += "\n";
                    motherstr += childstr;

                }
                //motherstr+="\n";
            }
            // Non-wildanimal section
        } else {
            /* So the principle of how this codes work is essentially the same as the handler on wildanimal. The only difference is  just
             the type of the animal itself ( because the system required that wildanimal cages will be different than non-wildanimal )*/
            if (animalist.size() == 1) {
                ArrayList<Cage> cagelvl1 = new ArrayList<>(Arrays.asList(new Cage(animalist.get(0))));
                zoocollection.set(1, cagelvl1);

                if (animalist.get(0).getLenght() < 45) {
                    motherstr += "location : indoor\n" +
                            "level 3:\n" +
                            "level 2:\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), " + "\n" +
                            "level 1: " + "\n\n";
                } else if (animalist.get(0).getLenght() >= 45 && animalist.get(0).getLenght() <= 60) {
                    motherstr += "location : indoor\n" +
                            "level 3:\n" +
                            "level 2:\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), " + "\n" +
                            "level 1: " + "\n\n";
                } else {
                    motherstr += "location : indoor\n" +
                            "level 3:\n" +
                            "level 2:\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), " + "\n" +
                            "level 1: " + "\n\n";
                }

            } else if (animalist.size() == 2) {
                ArrayList<Cage> cagelvl1 = new ArrayList<>(Arrays.asList(new Cage(animalist.get(0))));
                ArrayList<Cage> cagelvl2 = new ArrayList<>(Arrays.asList(new Cage(animalist.get(1))));
                zoocollection.set(1, cagelvl1);
                zoocollection.set(0, cagelvl2);
                if (animalist.get(0).getLenght() < 45) {
                    motherstr += "location : indoor\n" +
                            "level 3:\n" +
                            "level 2: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - A), " + "\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), \n\n" +
                            "After rearrangement...\n" +
                            "level 3: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - A), " + "\n" +
                            "level 2: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - A), " + "\n" +
                            "level 1: " + "\n\n";
                } else if (animalist.get(0).getLenght() >= 45 && animalist.get(0).getLenght() <= 60) {
                    motherstr += "location : indoor\n" +
                            "level 3:\n" +
                            "level 2: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - B), " + "\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), \n\n" +
                            "After rearrangement...\n" +
                            "level 3: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - B, " + "\n" +
                            "level 2: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - B), " + "\n" +
                            "level 1: " + "\n\n";
                } else {
                    motherstr += "location : indoor\n" +
                            "level 3:\n" +
                            "level 2: " + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - C), " + "\n" +
                            "level 1: " + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), \n\n" +
                            "After rearrangement...\n" +
                            "level 3:" + animalist.get(1).getName() + " ( " + animalist.get(1).getLenght() + " - C, " + "\n" +
                            "level 2:" + animalist.get(0).getName() + " ( " + animalist.get(0).getLenght() + " - C), " + "\n" +
                            "level 1: " + "\n\n";
                }


            }
            //motherstr+="location : indoor\n";
            else {
                for (int i = 1; i < 3; i++) {
                    ArrayList<Cage> levelcollection = new ArrayList<Cage>(animalist.size() / 3);
                    String childstr = "level " + i + ": ";

                    for (int j = 0; j < animalist.size() / 3; j++) {
                        if (animalist.get(index).getLenght() < 75) {
                            IndoorA cage = new IndoorA(animalist.get(index));
                            levelcollection.add(cage);
                            childstr += cage.info() + ", ";
                        } else if (75 <= animalist.get(index).getLenght() && animalist.get(index).getLenght() <= 90) {
                            IndoorB cage = new IndoorB(animalist.get(index));
                            levelcollection.add(cage);
                            childstr += cage.info() + ", ";

                        } else {
                            IndoorC cage = new IndoorC(animalist.get(index));
                            levelcollection.add(cage);
                            childstr += cage.info() + ", ";
                        }


                        index += 1;

                    }
                    motherstr = "\n" + childstr + motherstr;
                    Collections.reverse(levelcollection);
                    //zoocollection.add(levelcollection);

                    if (i == 1) zoocollection.set(1, levelcollection);
                    if (i == 2) zoocollection.set(0, levelcollection);


                }

                ArrayList<Cage> levelcollection = new ArrayList<Cage>(animalist.size() - index);
                String childstr = "level 3: ";
                int left = animalist.size() - index;

                for (int j = 0; j < left; j++) {
                    if (animalist.get(index).getLenght() < 75) {
                        IndoorA cage = new IndoorA(animalist.get(index));
                        levelcollection.add(cage);
                        childstr += cage.info() + ", ";
                    } else if (75 <= animalist.get(index).getLenght() && animalist.get(index).getLenght() <= 90) {
                        IndoorB cage = new IndoorB(animalist.get(index));
                        levelcollection.add(cage);
                        childstr += cage.info() + ", ";

                    } else {
                        IndoorC cage = new IndoorC(animalist.get(index));
                        levelcollection.add(cage);
                        childstr += cage.info() + ", ";
                    }


                    index += 1;
                }

                Collections.reverse(levelcollection);

                zoocollection.set(2, levelcollection);

                motherstr = childstr + motherstr;
                motherstr = "location : indoor\n" + motherstr;


                motherstr += "\n\nAfter rearrangement...\n";
                for (int i = 0; i < 3; i++) {
                    int level = 3 - i;
                    childstr = "level " + level + ": ";
                    for (int j = 0; j < zoocollection.get(i).size(); j++) {
                        if (zoocollection.get(i).get(j).getAnimal().getLenght() < 75) {
                            childstr += zoocollection.get(i).get(j).getAnimal().getName() + " (" +
                                    zoocollection.get(i).get(j).getAnimal().getLenght() + " - A), ";
                        } else if (zoocollection.get(i).get(j).getAnimal().getLenght() <= 90 && zoocollection.get(i).get(j).getAnimal().getLenght() >= 75) {
                            childstr += zoocollection.get(i).get(j).getAnimal().getName() + " (" +
                                    zoocollection.get(i).get(j).getAnimal().getLenght() + " - B), ";
                        } else {
                            childstr += zoocollection.get(i).get(j).getAnimal().getName() + " (" +
                                    zoocollection.get(i).get(j).getAnimal().getLenght() + " - C), ";
                        }


                    }


                    childstr += "\n";
                    motherstr += childstr;

                }


            }


        }

        return motherstr;

    }
}
