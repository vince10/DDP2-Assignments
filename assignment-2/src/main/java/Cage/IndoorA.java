package Cage;

import Animals.*;

public class IndoorA extends Cage {
    private final long width = 60;
    private final long length = 60;

    public IndoorA(Animals animals) {
        super(animals);
    }

    public String info() {
        return getAnimal().getName() + " ( " + getAnimal().getLenght() + " - A)";
    }
}
