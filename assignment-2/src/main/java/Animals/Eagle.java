package Animals;

public class Eagle extends Animals {
    private boolean wildanimal = true;

    public Eagle(String name, int length) {
        super(name, length);
    }

    public boolean isWildanimal() {
        return wildanimal;
    }

    public String fly() {
        return this.getName() + " makes a voice: kwaakk…\n" +
                "You hurt!\n" + "Back to the office!";
    }

}
