package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class SectionsReader extends CsvReader {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public SectionsReader(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        //int count =0;
        List<String> valid_categories =  new ArrayList<>();
        for (String s : getLines()){
            String[] temp = s.split(",");
            switch (temp[2]){
                case "Explore the Mammals":
                    if (temp[1].equals("mammals")&&!valid_categories.contains(temp[2])){
                        valid_categories.add(temp[2]);
                    }
                case "World of Aves":
                    if (temp[1].equals("aves")&&!valid_categories.contains(temp[2])){
                        valid_categories.add(temp[2]);
                    }
                case "Reptillian Kingdom":
                    if (temp[1].equals("reptiles")&&!valid_categories.contains(temp[2])){
                        valid_categories.add(temp[2]);
                    }

            }
        }
        return valid_categories.size();
    }

    @Override
    public long countInvalidRecords() {
        return 3-countValidRecords();
    }
}
