package javari.animal;

public class Reptile extends Animal {
    String specialcase = "";
    public Reptile(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }
    public Reptile(Integer id, String type, String name, Gender gender, double length, double weight,String specialcase, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialcase=specialcase;
    }

    public Reptile(){};

    @Override
    protected boolean specificCondition() {
        if (specialcase.equals("wild")) return true;
        return false;
    }
}
